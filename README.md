# Specyfikacja

_PyVISA_Instruments_ jest zbiorem prostych bibliotek w języku Python do wysokopoziomowej obsługi różnych przyrządów pomiarowych, kontrolnych i zasilających.

Obsługiwane typy urządzeń:

- DMM Keithley DMM6500
- Oscilloscope Keysight DSOX1102A
- Power supply TTi QL355P
- Signal generator Siglent SDG 2042X
- Spectrum analyzer Siglent SSA 3021X

Platforma:
- Windows 10
- Python 3.12
- PyVISA 1.14
- Intefejsy komunikacyjne: USB-TMC lub Ethernet LXI

# Przykłady

```python
import logging
import time

import PyVISA_Instruments.visa_common
import PyVISA_Instruments.power_supply_ql355p as power_supply

logging.basicConfig(level=logging.INFO, format='[%(asctime)s.%(msecs)03d] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s', datefmt='%H:%M:%S')

use_ethernet = True

if use_ethernet:
    # Interface #1: Ethernet
    VISA_RESOURCE_POWER_SUPPLY = "TCPIP0::192.168.0.151::9221::SOCKET"
else:
    # Interface #2: USB (COM13)
    VISA_RESOURCE_POWER_SUPPLY = "ASRL13::INSTR"

visa_resource_manager = PyVISA_Instruments.visa_common.initialize_visa(list_resources=True)
power_supply.initialize_power_supply(visa_resource_manager, VISA_RESOURCE_POWER_SUPPLY)

power_supply.set_voltage_current(2.5, 0.1)
power_supply.set_output_state(1)
time.sleep(1.0)
power_supply.set_output_state(0)
```

# Autor

Woj. Rus. rwxrwx@interia.pl

# Licencja

MIT
