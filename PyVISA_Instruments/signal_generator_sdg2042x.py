"""
Signal generator Siglent SDG 2042X.
"""

import logging

import pyvisa as visa

# Script globals.
signal_generator = None

# Instrument parameters.
SINE_FREQUENCY_MIN_Hz = 1e-6
SINE_FREQUENCY_MAX_Hz = 120e6
SQUARE_FREQUENCY_MIN_Hz = 1e-6
SQUARE_FREQUENCY_MAX_Hz = 25e6
SQUARE_DUTY_CYCLE_MIN_PERCENT = 1e-3
SQUARE_DUTY_CYCLE_MAX_PERCENT = 99.999
ARB_SAMPLE_VALUE_MIN = -2**15
ARB_SAMPLE_VALUE_MAX = +2**15 - 1
ARB_SAMPLE_SIZEOF = 2
ARB_WAVEFORM_LENGTH_MIN_SAMPLES = 8
ARB_WAVEFORM_LENGTH_MAX_SAMPLES = 8 * 1000 * 1000
ARB_WAVEFORM_LENGTH_MIN_BYTES = ARB_SAMPLE_SIZEOF * ARB_WAVEFORM_LENGTH_MIN_SAMPLES
ARB_WAVEFORM_LENGTH_MAX_BYTES = ARB_SAMPLE_SIZEOF * ARB_WAVEFORM_LENGTH_MAX_SAMPLES

def initialize_signal_generator(visa_resource_manager: visa.ResourceManager, visa_resource_name: str) -> None:
    """
    Initialize instrument.
    """
    global signal_generator

    logging.info("Open VISA resource: " + visa_resource_name)
    signal_generator = visa_resource_manager.open_resource(visa_resource_name)
    signal_generator.timeout = 20 * 1000
    signal_generator.delay = 0.0
    signal_generator.read_termination = ""
    signal_generator.write_termination = ""
    signal_generator.chunk_size = ARB_WAVEFORM_LENGTH_MAX_BYTES + 1024

    logging.info("Query IDN")
    idn = signal_generator.query("*IDN?")
    logging.info("Device IDN: " + idn)

def check_white_spaces(s: str) -> None:
    """
    Check presence of white spaces in string.
    """
    for c in s:
        if c.isspace():
            raise ValueError("Invalid waveform name")

def reset() -> None:
    """
    The *RST command initiates a device reset and recalls the default setup.
    """
    command = "*RST"

    logging.info(command)
    signal_generator.write(command)

def set_output(channel: int,
               state: int,
               load: int,
               polarity: int) -> None:
    """
    Set output parameters.

    channel:
        1 = channel 1
        2 = channel 2
    state:
        0 = OFF
        1 = ON
    load: output resistance in Ohm:
        Values 0 ... 50 Ohm = effective 50 Ohm
        Values > 50 Ohm = effective High-Z
    polarity:
        0 = normal
        1 = inverted
    """
    channel = int(channel)
    state = int(state)
    load = int(load)
    polarity = int(polarity)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if state not in (0, 1):
        raise ValueError("Invalid state")

    if polarity not in (0, 1):
        raise ValueError("Invalid polarity")

    state_name = ("OFF", "ON")

    if load <= 50:
        load_name = "50"
    else:
        load_name = "HZ"

    polarity_name = ("NOR", "INVT")

    command = f"C{channel}:OUTP {state_name[state]},LOAD,{load_name},PLRT,{polarity_name[polarity]}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_sine(channel: int,
                  frequency: float,
                  period: float,
                  amplitude_type: str,
                  amplitude: float,
                  offset: float,
                  phase: float) -> None:
    """
    Set sine wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    frequency:
        If set frequency: 1 ... 120e6 Hz
        If use "period" instead: None
    period:
        If set period: TODO ... TODO s
        If use "frequency" instead: None
    amplitude_type:
        "vpp" = Vpp
        "vrms" = Vrms
        "dbm" = dBm
    amplitude:
        TODO
    offset:
        TODO ... TODO V
    phase:
        0,0 ... 360,0 degree
    """
    channel = int(channel)
    amplitude_type = str(amplitude_type).lower()
    amplitude = float(amplitude)
    offset = float(offset)
    phase = float(phase)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if frequency is not None:
        frequency = float(frequency)

    if period is not None:
        period = float(period)

    if ((frequency is None) and (period is None)) or \
        ((frequency is not None) and (period is not None)):
        raise ValueError("Invalid frequency or period")

    if frequency is not None:
        frequency_or_period_str = f"FRQ,{frequency:.6e}"
    else:
        frequency_or_period_str = f"PERI,{period:.6e}"

    command = f"C{channel}:BSWV WVTP,SINE,{frequency_or_period_str},OFST,{offset:.3e},PHSE,{phase:.3e}"

    if amplitude_type not in ("vpp", "vrms", "dbm"):
        raise ValueError("Invalid amplitude type")

    if amplitude_type == "vpp":
        command += ",AMP"
    elif amplitude_type == "vrms":
        command += ",AMPVRMS"
    elif amplitude_type == "dbm":
        command += ",AMPDBM"

    command += f",{amplitude:.3e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_square(channel: int,
                    frequency: float,
                    period: float,
                    amplitude_type: str,
                    amplitude: float,
                    offset: float,
                    phase: float,
                    duty: float) -> None:
    """
    Set square wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    frequency:
        If set frequency: 1 ... 120e6 Hz
        If use "period" instead: None
    period:
        If set period: TODO ... TODO s
        If use "frequency" instead: None
    amplitude_type:
        "vpp" = Vpp
        "vrms" = Vrms
        "dbm" = dBm
    amplitude:
        TODO
    offset:
        TODO ... TODO V
    phase:
        0,0 ... 360,0 degree
    duty:
        0,0 ... 100,0 %
    """
    channel = int(channel)
    amplitude_type = str(amplitude_type).lower()
    amplitude = float(amplitude)
    offset = float(offset)
    phase = float(phase)
    duty = float(duty)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if frequency is not None:
        frequency = float(frequency)

    if period is not None:
        period = float(period)

    if ((frequency is None) and (period is None)) or \
        ((frequency is not None) and (period is not None)):
        raise ValueError("Invalid frequency or period")

    if frequency is not None:
        frequency_or_period_str = f"FRQ,{frequency:.6e}"
    else:
        frequency_or_period_str = f"PERI,{period:.6e}"

    command = f"C{channel}:BSWV WVTP,SQUARE,{frequency_or_period_str},OFST,{offset:.3e},PHSE,{phase:.3e}"

    if amplitude_type not in ("vpp", "vrms", "dbm"):
        raise ValueError("Invalid amplitude type")

    if amplitude_type == "vpp":
        command += ",AMP"
    elif amplitude_type == "vrms":
        command += ",AMPVRMS"
    elif amplitude_type == "dbm":
        command += ",AMPDBM"

    command += f",{amplitude:.3e}"

    command += f",DUTY,{duty:.3e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_dc(channel: int, offset: float) -> None:
    """
    Set square wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    offset:
        TODO ... TODO V
    """
    channel = int(channel)
    offset = float(offset)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    command = f"C{channel}:BSWV WVTP,DC,OFST,{offset:.3e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_pulse(channel: int,
                   frequency: float,
                   period: float,
                   amplitude_type: str,
                   amplitude: float,
                   offset: float,
                   duty: float,
                   width: float,
                   rise: float,
                   fall: float,
                   delay: float) -> None:
    """
    Set pulse wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    frequency:
        If set frequency: 1 ... 120e6 Hz
        If use "period" instead: None
    period:
        If set period: TODO ... TODO s
        If use "frequency" instead: None
    amplitude_type:
        "vpp" = Vpp
        "vrms" = Vrms
        "dbm" = dBm
    amplitude:
        TODO
    offset:
        TODO ... TODO V
    duty:
        0,0 ... 100,0 %
    width:
        TODO
    rise:
        TODO
    fall:
        TODO
    delay:
        0,0 ... 1e-3 s
    """
    channel = int(channel)
    amplitude_type = str(amplitude_type).lower()
    amplitude = float(amplitude)
    offset = float(offset)
    rise = float(rise)
    fall = float(fall)
    delay = float(delay)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if frequency is not None:
        frequency = float(frequency)

    if period is not None:
        period = float(period)

    if ((frequency is None) and (period is None)) or \
        ((frequency is not None) and (period is not None)):
        raise ValueError("Invalid frequency or period")

    if frequency is not None:
        frequency_or_period_str = f"FRQ,{frequency:.6e}"
    else:
        frequency_or_period_str = f"PERI,{period:.6e}"

    command = f"C{channel}:BSWV WVTP,PULSE,{frequency_or_period_str},OFST,{offset:.3e}"

    if amplitude_type not in ("vpp", "vrms", "dbm"):
        raise ValueError("Invalid amplitude type")

    if amplitude_type == "vpp":
        command += ",AMP"
    elif amplitude_type == "vrms":
        command += ",AMPVRMS"
    elif amplitude_type == "dbm":
        command += ",AMPDBM"

    command += f",{amplitude:.3e}"

    if duty is not None:
        duty = float(duty)

    if width is not None:
        width = float(width)

    if ((duty is None) and (width is None)) or \
        ((duty is not None) and (width is not None)):
        raise ValueError("Invalid duty or width")

    if duty is not None:
        duty_or_width_str = f"DUTY,{duty:.3e}"
    else:
        duty_or_width_str = f"WIDTH,{width:.3e}"

    command += f",{duty_or_width_str}"

    command += f",RISE,{rise:.3e}"
    command += f",FALL,{fall:.3e}"
    command += f",DLY,{delay:.6e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_ramp(channel: int,
                  frequency: float,
                  period: float,
                  amplitude_type: str,
                  amplitude: float,
                  offset: float,
                  phase: float,
                  symmetry: float) -> None:
    """
    Set ramp wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    frequency:
        If set frequency: 1 ... 120e6 Hz
        If use "period" instead: None
    period:
        If set period: TODO ... TODO s
        If use "frequency" instead: None
    amplitude_type:
        "vpp" = Vpp
        "vrms" = Vrms
        "dbm" = dBm
    amplitude:
        TODO
    offset:
        TODO ... TODO V
    phase:
        0,0 ... 360,0 degree
    symmetry:
        0,0 ... 100,0 percent
    """
    channel = int(channel)
    amplitude_type = str(amplitude_type).lower()
    amplitude = float(amplitude)
    offset = float(offset)
    phase = float(phase)
    symmetry = float(symmetry)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if frequency is not None:
        frequency = float(frequency)

    if period is not None:
        period = float(period)

    if ((frequency is None) and (period is None)) or \
        ((frequency is not None) and (period is not None)):
        raise ValueError("Invalid frequency or period")

    if frequency is not None:
        frequency_or_period_str = f"FRQ,{frequency:.6e}"
    else:
        frequency_or_period_str = f"PERI,{period:.6e}"

    command = f"C{channel}:BSWV WVTP,RAMP,{frequency_or_period_str},OFST,{offset:.3e},PHSE,{phase:.3e}"

    if amplitude_type not in ("vpp", "vrms", "dbm"):
        raise ValueError("Invalid amplitude type")

    if amplitude_type == "vpp":
        command += ",AMP"
    elif amplitude_type == "vrms":
        command += ",AMPVRMS"
    elif amplitude_type == "dbm":
        command += ",AMPDBM"

    command += f",{amplitude:.3e}"

    command += f",SYM,{symmetry:.3e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_noise(channel: int,
                   std_dev: float,
                   mean: float,
                   bandwidth_state: int,
                   bandwidth_value: float) -> None:
    """
    Set noise wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    std_dev:
        TODO
    mean:
        TODO
    bandwidth_state:
        TODO
    bandwidth_value:
        TODO
    """
    channel = int(channel)
    std_dev = float(std_dev)
    mean = float(mean)
    bandwidth_state = int(bandwidth_state)
    bandwidth_value = float(bandwidth_value)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if bandwidth_state == 0:
        bandwidth_state_str = "OFF"
    else:
        bandwidth_state_str = "ON"

    command = f"C{channel}:BSWV WVTP,NOISE,STDEV,{std_dev:.3e},MEAN,{mean:.3e},BANDSTATE,{bandwidth_state_str},BANDWIDTH,{bandwidth_value:.6e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_arb(channel: int,
                 frequency: float,
                 period: float,
                 amplitude: float,
                 offset: float,
                 phase: float) -> None:
    """
    Set arbitrary wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    frequency:
        If set frequency: 1 ... 120e6 Hz
        If use "period" instead: None
    period:
        If set period: TODO ... TODO s
        If use "frequency" instead: None
    amplitude:
        TODO
    offset:
        TODO ... TODO V
    phase:
        0,0 ... 360,0 degree
    """
    channel = int(channel)
    amplitude = float(amplitude)
    offset = float(offset)
    phase = float(phase)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if frequency is not None:
        frequency = float(frequency)

    if period is not None:
        period = float(period)

    if ((frequency is None) and (period is None)) or \
        ((frequency is not None) and (period is not None)):
        raise ValueError("Invalid frequency or period")

    if frequency is not None:
        frequency_or_period_str = f"FRQ,{frequency:.6e}"
    else:
        frequency_or_period_str = f"PERI,{period:.6e}"

    command = f"C{channel}:BSWV WVTP,ARB,{frequency_or_period_str},OFST,{offset:.3e},PHSE,{phase:.3e}"

    command += f",AMP,{amplitude:.3e}"

    logging.info(command)
    signal_generator.write(command)

def select_wave_arb_builtin(channel: int, waveform_index: int) -> None:
    """
    TODO
    """
    channel = int(channel)
    waveform_index = int(waveform_index)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    command = f"C{channel}:ARWV INDEX,{waveform_index}"

    logging.info(command)
    signal_generator.write(command)

def select_wave_arb_user_defined(channel: int, waveform_name: str) -> None:
    """
    TODO
    """
    channel = int(channel)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    check_white_spaces(waveform_name)

    command = f"C{channel}:ARWV NAME,{waveform_name}"

    logging.info(command)
    signal_generator.write(command)

def read_wave_arb(waveform_name: str) -> None:
    """
    Read user arbitrary waveform from SG NV memory.

    waveform_name:
        Waveform name (without whitespaces).
    """
    check_white_spaces(waveform_name)

    command = f"WVDT? user,{waveform_name}"

    logging.info(command)
    signal_generator.write(command)
    data = signal_generator.read_raw()

    # TODO: parsowanie nagłówka i wycięcie danych binarnych.
    file_name = f"wave_arb_{waveform_name}.bin"
    f = open(file_name, "wb")
    f.write(data)
    f.close()

def write_wave_arb(channel: int,
                   waveform_name: str,
                   waveform_data: list,
                   frequency: float,
                   amplitude: float,
                   offset: float,
                   phase: float) -> None:
    """
    Write user arbitrary waveform to SG NV memory and set wave parameters.

    channel:
        1 = channel 1
        2 = channel 2
    waveform_name:
        Waveform name (without whitespaces).
    waveform_data:
        Waveform data (list of int16_t samples, length: 2 ... 8 * 2^20 samples).
    frequency:
        TODO Hz
    amplitude:
        TODO V
    offset:
        TODO ... TODO V
    phase:
        0,0 ... 360,0 degree
    """
    channel = int(channel)
    frequency = float(frequency)
    amplitude = float(amplitude)
    offset = float(offset)
    phase = float(phase)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    check_white_spaces(waveform_name)

    if not isinstance(waveform_data, list):
        raise TypeError("Require list")

    for sample in waveform_data:
        if not isinstance(sample, int):
            raise TypeError("Require int")
        if sample < ARB_SAMPLE_VALUE_MIN or sample > ARB_SAMPLE_VALUE_MAX:
            raise ValueError("Sample value out of range (require int16_t values)")

    if len(waveform_data) < ARB_WAVEFORM_LENGTH_MIN_SAMPLES or len(waveform_data) > ARB_WAVEFORM_LENGTH_MAX_SAMPLES:
        raise ValueError(f"Invalid sample number in waveform (require range: {ARB_WAVEFORM_LENGTH_MIN_SAMPLES} ... {ARB_WAVEFORM_LENGTH_MAX_SAMPLES} samples)")

    command = f"C{channel}:WVDT WVNM,{waveform_name},LENGTH,{len(waveform_data) * ARB_SAMPLE_SIZEOF}"
    command += f",FREQ,{frequency:.6e},AMPL,{amplitude:.3e},OFST,{offset:.3e},PHASE,{phase:.3e}"
    command += ",WAVEDATA,"

    logging.info(command + f"[+ {len(waveform_data)} waveform samples]")
    signal_generator.write_binary_values(command, waveform_data, datatype='h', is_big_endian=False, header_fmt='empty')

def set_sample_rate_arb(channel: int,
                        mode: str,
                        sample_rate: float) -> None:
    """
    Set mode and sample rate for arbitrary waweform.

    TODO/IMPORTANT: after change dds->arb SCPI answer is fast but instrument processing is very slow (freeze state) for 8M samples.

    channel:
        1 = channel 1
        2 = channel 2
    mode:
        "dds" = DDS mode.
        "tarb" = TrueArb mode.
    sample_rate:
        Waveform sample rate. The unit is Sa/s.
    """
    channel = int(channel)
    sample_rate = float(sample_rate)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    if mode == "dds":
        mode_name = "DDS"
    elif mode == "tarb":
        mode_name = "TARB"
    else:
        raise ValueError("Invalid mode")

    command = f"C{channel}:SRATE MODE,{mode_name},VALUE,{sample_rate:.6e}"

    logging.info(command)
    signal_generator.write(command)

def set_wave_level(channel: int,
                   low_level: float,
                   high_level: float) -> None:
    """
    Set wave amplitude by low/high levels.

    channel:
        1 = channel 1
        2 = channel 2
    low_level:
        -10,0 ... +10,0 V
    high_level:
        -10,0 ... +10,0 V
    """
    channel = int(channel)
    low_level = float(low_level)
    high_level = float(high_level)

    if channel not in (1, 2):
        raise ValueError("Invalid channel")

    command = f"C{channel}:BSWV LLEV,{low_level:.3e},HLEV,{high_level:.3e}"

    logging.info(command)
    signal_generator.write(command)

def save_screenshot() -> None:
    """
    Save screenshot.
    """
    command = "SCDP"

    logging.info(command)
    signal_generator.write(command)
    image_data = signal_generator.read_raw()
    
    f = open("screenshot_sdg2042x.bmp", "wb")
    f.write(image_data)
    f.close()
