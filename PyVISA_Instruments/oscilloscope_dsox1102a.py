"""
Oscilloscope Keysight DSOX1102A.

Source: Keysight InfiniiVision 1000 X-Series Oscilloscopes Programmer's Guide, Version 01.20.0000
"""

import struct
import logging

import pyvisa as visa

# Script globals.
oscilloscope = None

def initialize_oscilloscope(visa_resource_manager: visa.ResourceManager, visa_resource_name: str) -> None:
    """
    Initialize instrument.
    """    
    global oscilloscope

    logging.info("Open VISA resource: " + visa_resource_name)
    oscilloscope = visa_resource_manager.open_resource(visa_resource_name)
    oscilloscope.timeout = 10000
    oscilloscope.read_termination = ""
    oscilloscope.write_termination = ""
    oscilloscope.clear()

    logging.info("Query IDN")
    idn = oscilloscope.query("*IDN?")
    logging.info("Device IDN: " + idn)

def get_binary_data_from_ieee_block(data_in):
    """
    Source: InfiniiVision_Save_ScreenShot_to_PC_Python-3.5.py
    """
    # Grab the beginning section of data, which will contain the header.
    Header = str(data_in[0 : 12])
    logging.info("IEEE block: header: " + str(Header))
    
    # Find the start position of the IEEE header, which starts with a '#'.
    startpos = Header.find("#")
    logging.info("IEEE block: header start position: " + str(startpos))
    
    # Check for problem with start position.
    if startpos < 0:
        raise IOError("No start of block found")
        
    # Find the number that follows '#' symbol.  This is the number of digits in the block length.
    Size_of_Length = int(Header[startpos + 1])
    logging.info("IEEE block: size of 'Length' field: " + str(Size_of_Length))
    
    # Now that we know how many digits are in the size value, get the size of the payload.
    Payload_Size = int(Header[startpos + 2 : startpos + 2 + Size_of_Length])
    logging.info("IEEE block: binary payload length: " + str(Payload_Size))
    
    # Get the length from the header
    offset = startpos + Size_of_Length
    
    # Extract the data out into a list.
    return data_in[offset : offset + Payload_Size]

def save_screenshot() -> None:
    """
    Reads screen image data.
    """
    oscilloscope.write(":HARDcopy:INKSaver 0")
    oscilloscope.write(":DISPlay:DATA? PNG, COLor")
    ieee_block = oscilloscope.read_raw()
    image_data = get_binary_data_from_ieee_block(ieee_block)
    
    f = open("screenshot_dsox1102a.png", "wb")
    f.write(image_data)
    f.close()

def enable_demo(signal: str, output_state: bool) -> None:
    """
    Enable demo.
    
    <signal> ::= {SINusoid | NOISy | LFSine | AM | RFBurst | FMBurst
                 | HARMonics | COUPling | RINGing | SINGle | CLK | TRANsition
                 | BURSt | GLITch | UART | CAN | LIN}
    """
    output_state = int(output_state)
    
    if output_state:
        oscilloscope.write(f":DEMO:FUNCtion {signal}")
        oscilloscope.write(":DEMO:OUTPut 1")
    else:
        oscilloscope.write(":DEMO:OUTPut 0")

def system_display_string(s: str) -> None:
    """
    Writes the string to a text box in the center of the display.
    """
    STRING_LENGTH_MAX = 75

    s = str(s)
    if len(s) > STRING_LENGTH_MAX:
        s = s[0 : STRING_LENGTH_MAX]

    oscilloscope.write(f':SYSTem:DSP "{str(s)}"')

def system_set_date(year: int, month: int, day_of_month: int) -> None:
    """
    <year> ::= 4-digit year in NR1 format
    <month> ::= {1,..,12 | JANuary | FEBruary | MARch | APRil | MAY | JUNe
                         | JULy | AUGust | SEPtember | OCTober | NOVember | DECember}
    <day> ::= {1,..,31}
    """
    oscilloscope.write(f":SYSTem:DATE {int(year)},{int(month)},{int(day_of_month)}")

def system_set_time(hours: int, minutes: int, seconds: int) -> None:
    """
    The :SYSTem:TIME command sets the system time, using a 24-hour format.\n
    Commas are used as separators. Validity checking is performed to ensure that the
    time is valid.
    """
    oscilloscope.write(f":SYSTem:TIME {int(hours)},{int(minutes)},{int(seconds)}")

def system_lock(state: bool) -> None:
    """
    Disables the front panel.
    """
    state = int(state)

    if state not in (0, 1):
        raise ValueError("Invalid state")

    oscilloscope.write(f":SYSTem:LOCK {state}")

def system_preset() -> None:
    """
    The :SYSTem:PRESet command places the instrument in a known state. This is the
    same as pressing the [Default Setup] key or [Save/Recall] > Default/Erase > Default
    Setup on the front panel.
    When you perform a default setup, some user settings (like preferences) remain
    unchanged. To reset all user settings to their factory defaults, use the *RST
    command.
    """
    oscilloscope.write(":SYSTem:PRESet")

def set_channel(channel_num: int,
                display: bool,
                offset: float,
                scale: float,
                probe: float = 1.0) -> None:
    """
    Configure analog channels.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    display = int(display)
    offset = float(offset)
    scale = float(scale)
    probe = float(probe)

    oscilloscope.write(f":CHANnel{channel_num}:BANDwidth 200E6")            # available BW limit is 25E6
    oscilloscope.write(f":CHANnel{channel_num}:BWLimit 0")                  # <bwlimit> ::= {{1 | ON} | {0 | OFF}
    oscilloscope.write(f":CHANnel{channel_num}:COUPling DC")                # <coupling> ::= {AC | DC}
    oscilloscope.write(f":CHANnel{channel_num}:DISPlay {display}")          # <display value> ::= {{1 | ON} | {0 | OFF}}
    oscilloscope.write(f":CHANnel{channel_num}:IMPedance ONEMeg")           # <impedance> ::= ONEMeg
    oscilloscope.write(f":CHANnel{channel_num}:INVert 0")                   # <invert value> ::= {{1 | ON} | {0 | OFF}
    oscilloscope.write(f':CHANnel{channel_num}:LABel "{channel_num}"')      # <string> ::= quoted ASCII string (max 10 chars.)
    oscilloscope.write(f":CHANnel{channel_num}:OFFSet {offset:.4e} V")      # <offset> ::= Vertical offset value in NR3 format / <suffix> ::= {V | mV}
    oscilloscope.write(f":CHANnel{channel_num}:PROBe {probe:.4e}")          # <attenuation> ::= probe attenuation ratio in NR3 format
    oscilloscope.write(f":CHANnel{channel_num}:PROBe:SKEW 0")               # <skew value> ::= skew time in NR3 format (-100 ns to +100 ns)
    oscilloscope.write(f":CHANnel{channel_num}:SCALe {scale:.4e} V")        # <scale> ::= vertical units per division in NR3 format / <suffix> ::= {V | mV}  
    oscilloscope.write(f":CHANnel{channel_num}:UNITs VOLT")                 # <units> ::= {VOLT | AMPere}
    oscilloscope.write(f":CHANnel{channel_num}:VERNier 0")                  # <vernier value> ::= {{1 | ON} | {0 | OFF}

def set_acquire() -> None:
    """
    Configure acquire.
    """
    oscilloscope.write(":ACQuire:MODE RTIMe")           # <mode> ::= {RTIMe | SEGMented}
    oscilloscope.write(":ACQuire:TYPE NORMal")          # <type> ::= {NORMal | AVERage | HRESolution | PEAK}
    oscilloscope.write(":ACQuire:COUNt 16")             # for type = AVERage (1...65536)

def set_trigger() -> None:
    """
    Configure trigger.
    """
    oscilloscope.write(":TRIGger:NREJect 0")                # <value> ::= {{0 | OFF} | {1 | ON}}
    oscilloscope.write(":TRIGger:HFReject 0")               # <value> ::= {{0 | OFF} | {1 | ON}}, adds a 50 kHz low-pass filter in the trigger path
    oscilloscope.write(":TRIGger:HOLDoff +60e-9")           # <holdoff_time> ::= 60 ns to 10 s in NR3 format
    oscilloscope.write(":TRIGger:MODE EDGE")                # <mode> ::= {EDGE | GLITch | PATTern | SHOLd | TRANsition | TV | SBUS1}
    oscilloscope.write(":TRIGger:SWEep AUTO")               # <sweep> ::= {AUTO | NORMal}. This feature is called "Mode" on the instrument's front panel.
    oscilloscope.write(":TRIGger:EDGE:COUPling DC")         # <coupling> ::= {AC | DC | LFReject}
    oscilloscope.write(":TRIGger:EDGE:LEVel +2.0")          # <level> ::= 0.75 x full-scale voltage from center screen NR3 format for internal triggers, ±(external range setting) in NR3 format for external triggers
    oscilloscope.write(":TRIGger:EDGE:REJect OFF")          # <reject> ::= {OFF | LFReject | HFReject}
    oscilloscope.write(":TRIGger:EDGE:SLOPe POSitive")      # <slope> ::= {NEGative | POSitive | EITHer | ALTernate}
    oscilloscope.write(":TRIGger:EDGE:SOURce CHANnel1")     # <source> ::= {CHANnel<n> | EXTernal | LINE}

def trigger_force() -> None:
    """
    Command causes an acquisition to be captured even though
    the trigger condition has not been met. This command is equivalent to the front
    panel [Force Trigger] key.
    """
    oscilloscope.write(":TRIGger:FORCe")

def set_trigger_external(display: int,
                         trigger_range: float = 8.0,
                         level: float = 1.0,
                         position: float = -3.5) -> None:
    """
    Configure external trigger.
    """
    oscilloscope.write(f":EXTernal:DISPlay {int(display)}")             # <setting> ::= {0 | 1}. The command turns the external trigger input display on or off.
    oscilloscope.write(f":EXTernal:LEVel {float(level):.4e} V")         # <value> ::= external trigger level value in NR3 format. The command sets the external trigger input threshold (trigger) voltage level.
    oscilloscope.write(f":EXTernal:POSition {float(position):.4e}")     # <value> ::= Ext Trig waveform vertical position in divisions in NR3 format. Waveform can be positioned from -3.5 divisions to 2.5 divisions.
    oscilloscope.write(":EXTernal:PROBe 1.0")                           # <attenuation> ::= probe attenuation ratio in NR3 format. The probe attenuation factor may be 0.1 to 10000.
    oscilloscope.write(f":EXTernal:RANGe {float(trigger_range):.4e} V") # <range> ::= vertical full-scale range value in NR3 format (1.6 V or 8 V).
    oscilloscope.write(":EXTernal:UNITs VOLT")                          # <units> ::= {VOLT | AMPere}

def set_timebase(mode: str = "MAIN", scale: float = 1e-3) -> None:
    """
    Configure timebase.
    """
    mode = mode.lower()

    if mode not in ("main", "window", "wind", "xy", "roll"):
        raise ValueError("Invalid timebase mode")

    oscilloscope.write(f":TIMebase:SCALe {scale:.4e}")      # <scale_value> ::= time/div in seconds in NR3 format
    oscilloscope.write(f":TIMebase:MODE {mode}")            # <value> ::= {MAIN | WINDow | XY | ROLL}
    oscilloscope.write(":TIMebase:POSition 0")              # <pos> ::= time in seconds from the trigger to the display reference in NR3 format
    oscilloscope.write(":TIMebase:REFerence CENTer")        # <reference> ::= {LEFT | CENTer | RIGHt}
    oscilloscope.write(":TIMebase:VERNier 0")               # <vernier value> ::= {{1 | ON} | {0 | OFF}

def set_timebase_window(position: float = 100e-6, scale: float = 100e-6) -> None:
    """
    Precondition: ":TIMebase:MODE WINDow".
    """
    oscilloscope.write(f":TIMebase:WINDow:POSition {float(position):.4e}")     # <pos value> ::= time from the trigger event to the zoomed (delayed) view reference point in NR3 format
    oscilloscope.write(f":TIMebase:WINDow:SCALe {float(scale):.4e}")           # <scale_value> ::= scale value in seconds in NR3 format

def set_fft(display: bool,
            source: int,
            reference: float = 0,
            center: float = 50e3,
            span: float = 100e3,
            scale: float = 20.0) -> None:
    """
    Configure FFT.
    """
    oscilloscope.write(f":FFT:CENTer {float(center):.4e}")          # <frequency> ::= the current center frequency in NR3 format. The range of legal values is from -25 GHz to 25 GHz.
    oscilloscope.write(f":FFT:DISPlay {int(display)}")              # When ON is selected, the FFT function is calculated and displayed.
    oscilloscope.write(f":FFT:REFerence {float(reference):.4e}")    # <level> ::= the current reference level in NR3 format.
    oscilloscope.write(f":FFT:SCALe {float(scale):.4e} dB")         # <scale_value> ::= floating-point value in NR3 format.
    oscilloscope.write(f":FFT:SOURce1 CHANnel{int(source)}")        # <source> ::= {CHANnel<n>}, <n> ::= 1 to (# analog channels) in NR1 format.
    oscilloscope.write(f":FFT:SPAN {float(span):.4e}")              # <span> ::= the current frequency span in NR3 format. Legal values are 1 Hz to 100 GHz.
    oscilloscope.write(":FFT:VTYPe DECibel")                        # <units> ::= {DECibel | VRMS}
    oscilloscope.write(":FFT:WINDow HANNing")                       # <window> ::= {RECTangular | HANNing | FLATtop | BHARris}

def set_dvm(enable: bool, source: int, mode: str = "DC") -> None:
    """
    The :DVM:MODE command sets the digital voltmenter (DVM) mode:
    • ACRMs — displays the root-mean-square value of the acquired data, with the
              DC component removed.
    • DC — displays the DC value of the acquired data.
    • DCRMs — displays the root-mean-square value of the acquired data.
    • FREQuency — displays the frequency counter measurement. Requires the EDGE
                  or GLITch trigger mode, and the DVM source and the trigger source must be
                  the same analog channel.

    The :DVM:SOURce command sets the select the analog channel on which digital
    voltmeter (DVM) measurements are made.
    The selected channel does not have to be on (displaying a waveform) in order for
    DVM measurements to be made.
    """
    source = int(source)

    if source not in (1, 2):
        raise ValueError("Invalid source number")

    mode = mode.lower()

    if mode not in ("acrms", "acrm", "dc", "dcrms", "dcrm", "frequency", "freq"):
        raise ValueError("Invalid DVM mode")

    oscilloscope.write(":DVM:ARANge 0")                     # <setting> ::= {{OFF | 0} | {ON | 1}}. The command turns the digital voltmeter's Auto Range capability on or off.
    oscilloscope.write(f":DVM:ENABle {int(enable)}")        # <setting> ::= {{OFF | 0} | {ON | 1}}. The command turns the digital voltmeter (DVM) analysis feature on or off.
    oscilloscope.write(f":DVM:MODE {mode}")                 # <dvm_mode> ::= {ACRMs | DC | DCRMs | FREQuency}
    oscilloscope.write(f":DVM:SOURce CHANnel{source}")      # <source> ::= {CHANnel<n>}

def dvm_read_voltage() -> float:
    """
    The :DVM:CURRent? query returns the displayed 3-digit DVM value based on the
    current mode.
    It can take up to a few seconds after DVM analysis is enabled before this query starts to
    produce good results, that is, results other than +9.9E+37. To wait for good values after DVM
    analysis is enabled, programs should loop until a value less than +9.9E+37 is returned.
    """
    value = oscilloscope.query(":DVM:CURRent?")
    
    return float(value)

def dvm_read_frequency() -> float:
    """
    The :DVM:FREQuency? query returns the displayed 5-digit frequency value that is
    displayed below the main DVM value.
    If the requirements for the DVM FREQuency mode are not met (see ":DVM:MODE"
    on page 246), this query will return 9.9E+37.
    """
    value = oscilloscope.query(":DVM:FREQuency?")
    
    return float(value)

def acquisition_start(source: str = None) -> None:
    """
    Captures data that meets the specifications set up by the
    :ACQuire subsystem. When the digitize process is complete, the acquisition is
    stopped.
    """
    command = ":DIGitize"
    if source is not None:
        command += " " + source

    oscilloscope.write(command)     # <source> ::= {CHANnel<n> | FUNCtion | MATH | FFT | ABUS | EXT} (up to 5)

def save_waveform(channel_num: int, format_ascii: bool = False) -> None:
    """
    Save sampled data points.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    format_ascii = int(format_ascii)
    if format_ascii == 0:
        format_value = "WORD"
        file_ext = "bin"
    else:
        format_value = "ASCii"
        file_ext = "txt"

    oscilloscope.write(":WAVeform:BYTeorder LSBFirst")              # <value> ::= {LSBFirst | MSBFirst}. This command affects the transmitting sequence only when :WAVeform:FORMat WORD is selected.
    oscilloscope.write(":WAVeform:UNSigned 0")                      # <unsigned> ::= {{0 | OFF} | {1 | ON}}
    oscilloscope.write(f":WAVeform:FORMat {format_value}")          # <value> ::= {WORD | BYTE | ASCii}
    oscilloscope.write(":WAVeform:POINts:MODE MAXimum")             # <points_mode> ::= {NORMal | MAXimum | RAW}
    oscilloscope.write(f":WAVeform:SOURce CHANnel{channel_num}")    # <source> ::= {CHANnel<n> | FUNCtion | MATH | FFT | WMEMory<r> | ABUS | EXT}, n=1/2, r=1/2
    
    if format_ascii == 0:
        binary_data = oscilloscope.query_binary_values(":WAVeform:DATA?", datatype='h', is_big_endian=False, container=list)

        f = open(f"waveform.{file_ext}", "wb")
        file_data = struct.pack(f">{len(binary_data)}h", *binary_data)
        f.write(file_data)
        f.close()
    else:
        oscilloscope.write(":WAVeform:DATA?")
        ieee_block = oscilloscope.read_raw()
        binary_data = get_binary_data_from_ieee_block(ieee_block)

        f = open(f"waveform.{file_ext}", "wb")
        f.write(binary_data)
        f.close()

    preamble = oscilloscope.query(":WAVeform:PREamble?")
    f = open("waveform_preamble.txt", "w")
    f.write(preamble)
    f.close()

    preamble = preamble.split(",")
    preamble_format     = int(preamble[0])
    preamble_type       = int(preamble[1])
    preamble_points     = int(preamble[2])
    preamble_count      = int(preamble[3])
    preamble_xincrement = float(preamble[4])
    preamble_xorigin    = float(preamble[5])
    preamble_xreference = float(preamble[6])
    preamble_yincrement = float(preamble[7])
    preamble_yorigin    = float(preamble[8])
    preamble_yreference = float(preamble[9])

    logging.info(f"""Waveform preamble:
    format: {preamble_format}, type: {preamble_type}, points: {preamble_points}, count: {preamble_count},
    xincrement: {preamble_xincrement}, xorigin: {preamble_xorigin}, xreference: {preamble_xreference},
    yincrement: {preamble_yincrement}, yorigin: {preamble_yorigin}, yreference: {preamble_yreference}
    """)

    if format_ascii == 0:
        f = open("waveform.txt", "w")

        for sample in binary_data:
            sample = (float(sample) - preamble_yreference) * preamble_yincrement + preamble_yorigin
            f.write(f"{sample:.5e}\n")

        f.close()

def measure_clear() -> None:
    """
    Clears all selected measurements and markers from the screen.
    """
    oscilloscope.write(":MEASure:CLEar")

def measure_all() -> None:
    """
    Installs a Snapshot All measurement on the screen.
    """
    oscilloscope.write(":MEASure:ALL")

def measure_bit_rate(channel_num: int) -> None:
    """
    Installs a screen measurement and starts the bit
    rate measurement.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    oscilloscope.write(f":MEASure:BRATe CHANnel{channel_num}")

def measure_bit_rate_read(channel_num: int) -> float:
    """
    Measures all positive and negative pulse widths on
    the waveform, takes the minimum value found of either width type and inverts that
    minimum width to give a value in Hertz.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    value = oscilloscope.query(f":MEASure:BRATe? CHANnel{channel_num}")

    return float(value)

def measure_counter(channel_num: int) -> None:
    """
    Installs a screen measurement and starts a
    counter measurement. If the optional source parameter is specified, the current
    source is modified. Any channel except Math may be selected for the source.
    The counter measurement counts trigger level crossings at the selected trigger
    slope and displays the results in Hz.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    oscilloscope.write(f":MEASure:COUNter CHANnel{channel_num}")

def measure_counter_read(channel_num: int) -> float:
    """
    Measures and outputs the counter frequency of
    the specified source.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    value = oscilloscope.query(f":MEASure:COUNter? CHANnel{channel_num}")

    return float(value)

def measure_duty_cycle(channel_num: int) -> None:
    """
    Installs a screen measurement and starts a
    duty cycle measurement.
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    oscilloscope.write(f":MEASure:DUTYcycle CHANnel{channel_num}")

def measure_duty_cycle_read(channel_num: int) -> float:
    """
    Measures and outputs the duty cycle of the signal.
    The value returned for the
    duty cycle is the ratio of the positive pulse width to the period. The positive pulse
    width and the period of the specified signal are measured, then the duty cycle is
    calculated with the following formula:
    duty cycle = (+pulse width/period)*100
    """
    channel_num = int(channel_num)

    if channel_num not in (1, 2):
        raise ValueError("Invalid channel number")

    value = oscilloscope.query(f":MEASure:DUTYcycle? CHANnel{channel_num}")

    return float(value)
