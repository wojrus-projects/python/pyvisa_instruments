"""
Spectrum analyzer Siglent SSA 3021X.
"""

import logging

import pyvisa as visa

# Script globals.
spectrum_analyzer = None

def initialize_spectrum_analyzer(visa_resource_manager: visa.ResourceManager, visa_resource_name: str) -> None:
    """
    Initialize instrument.
    """
    global spectrum_analyzer

    logging.info("Open VISA resource: " + visa_resource_name)
    spectrum_analyzer = visa_resource_manager.open_resource(visa_resource_name)
    spectrum_analyzer.timeout = 10 * 1000
    spectrum_analyzer.delay = 0.0
    spectrum_analyzer.read_termination = ""
    spectrum_analyzer.write_termination = ""
    spectrum_analyzer.chunk_size = 4 * 1024 * 1024

    logging.info("Query IDN")
    idn = spectrum_analyzer.query("*IDN?")
    logging.info("Device IDN: " + idn)

def reset() -> None:
    """
    The *RST command initiates a device reset and recalls the default setup.
    """
    command = "*RST"

    logging.info(command)
    spectrum_analyzer.write(command)

def set_center_frequency(center_frequency: float) -> None:
    """
    Set center frequency (CF).
    """
    center_frequency = float(center_frequency)

    command = f":SENSe:FREQuency:CENTer {center_frequency:.9e}"

    logging.info(command)
    spectrum_analyzer.write(command)

def set_range_frequency(start_frequency: float, stop_frequency: float) -> None:
    """
    Set frequency range.
    """
    start_frequency = float(start_frequency)
    stop_frequency = float(stop_frequency)

    command = f":SENSe:FREQuency:STARt {start_frequency:.9e};STOP {stop_frequency:.9e}"

    logging.info(command)
    spectrum_analyzer.write(command)

def set_span(span: float) -> None:
    """
    Set frequency span.
    """
    span = float(span)

    command = f":SENSe:FREQuency:SPAN {span:.9e}"

    logging.info(command)
    spectrum_analyzer.write(command)

def save_screenshot() -> None:
    """
    Save screenshot.
    """
    command = "SCDP"

    logging.info(command)
    spectrum_analyzer.write(command)
    image_data = spectrum_analyzer.read_raw()
    
    f = open("screenshot_ssa3021x.bmp", "wb")
    f.write(image_data)
    f.close()

def power_off() -> None:
    """
    Turn off the instrument.
    """
    command = ":SYSTem:POWer:OFF"

    logging.info(command)
    spectrum_analyzer.write(command)

def set_input_attenuator(attenuation: int) -> None:
    """
    Sets the input attenuator of the spectrum analyzer.

    attenuation:
        0 ... 50 dB (default 20 dB)
    """
    attenuation = int(attenuation)

    if attenuation < 0 or attenuation > 50:
        raise ValueError("Invalid attenuation")

    command = f":SENSe:POWer:RF:ATTenuation {attenuation}"

    logging.info(command)
    spectrum_analyzer.write(command)

def enable_preamp(preamp_state: bool) -> None:
    """
    Turns the internal preamp on/off.

    preamp_state:
        False (or 0) = disabled
        True (or 1) = enabled
    """
    preamp_state = int(preamp_state)

    if preamp_state not in (0, 1):
        raise ValueError("Invalid preamp state")

    command = f":SENSe:POWer:RF:GAIN:STATe {preamp_state}"

    logging.info(command)
    spectrum_analyzer.write(command)

def set_reference_offset(offset: float) -> None:
    """
    Sets reference offset.

    offset:
        -300,0 ... +300,0 dB (default 0 dB)
    """
    offset = float(offset)

    if offset < -300.0 or offset > 300.0:
        raise ValueError("Invalid reference offset")

    command = f":DISPlay:WINDow:TRACe:Y:SCALe:RLEVel:OFFSet {offset:.9e}"

    logging.info(command)
    spectrum_analyzer.write(command)

def set_y_scale(scale_per_div: float) -> None:
    """
    Sets the per-division display scaling for the y-axis when scale type of Y axis is set to Log.

    scale_per_div:
        1,0 ... 20,0 dB (default 10,0 dB)
    """
    scale_per_div = float(scale_per_div)

    if scale_per_div < 1.0 or scale_per_div > 20.0:
        raise ValueError("Invalid Y scale")

    command = f":DISPlay:WINDow:TRACe:Y:SCALe:PDIVision {scale_per_div:.9e}"

    logging.info(command)
    spectrum_analyzer.write(command)

def query_trace_data(trace_number: int) -> list:
    """
    Returns the current displayed data (list[float]).

    trace_number:
        1 .. 4 (default 1)
    """
    trace_number = int(trace_number)

    if trace_number < 1 or trace_number > 4:
        raise ValueError("Invalid trace number")

    command = f":TRACe:DATA? {trace_number}"

    logging.info(command)
    data = spectrum_analyzer.query(command)

    data = data.split(',')
    data.pop()
    data = [float(value) for value in data]

    return data
