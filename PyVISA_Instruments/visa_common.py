"""
PyVISA common code.
"""

import logging

import pyvisa as visa

# Uncomment to enable VISA logging.
# visa.log_to_screen()

def initialize_visa(list_resources: bool = False) -> visa.ResourceManager:
    logging.debug("Initialize VISA")
    visa_resource_manager = visa.ResourceManager()
    logging.debug(visa_resource_manager)

    if list_resources:
        logging.debug(visa_resource_manager.list_resources())

    return visa_resource_manager
