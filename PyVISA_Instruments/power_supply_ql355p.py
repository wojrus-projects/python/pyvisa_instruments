"""
Power supply TTi QL355P.
"""

import logging

import pyvisa

# Script globals.
power_supply = None

def initialize_power_supply(visa_resource_manager: pyvisa.ResourceManager, visa_resource_name: str) -> None:
    """
    Initialize instrument.
    """
    global power_supply

    logging.info("Open VISA resource: " + visa_resource_name)
    power_supply = visa_resource_manager.open_resource(visa_resource_name)
    power_supply.timeout = 10000
    power_supply.read_termination = "\r\n"
    power_supply.write_termination = "\r\n"

    logging.info("Query IDN")
    idn = power_supply.query("*IDN?")
    logging.info("Device IDN: " + idn)

def set_voltage_current(v: float, i: float) -> None:
    """
    Set output voltage value and output current limit.
    
    :param v: Voltage [V] (format: 00.000).
    :param v: Current [A] (format: 0.0000).
    """
    logging.info(f"Set V: {v} [V], I: {i} [A]")

    command = f"V1 {v:.3f};I1 {i:.4f};*OPC?"
    power_supply.query(command)

def get_voltage_current() -> tuple[float, float]:
    """
    Measure output voltage and current.
    
    :return: (voltage [V], current [A])
    """
    power_supply.write("V1O?")
    value_raw = power_supply.read_raw()
    value_str = str(value_raw, 'utf-8')
    value_str = value_str.replace("V\r\n", "")
    v = float(value_str)

    power_supply.write("I1O?")
    value_raw = power_supply.read_raw()
    value_str = str(value_raw, 'utf-8')
    value_str = value_str.replace("A\r\n", "")
    i = float(value_str)

    logging.info(f"Get V: {v} [V], I: {i} [A]")

    return (v, i)

def set_output_state(state: bool) -> None:
    """
    Set output state.
    
    :param state: Output state.
    """
    state = int(state)
    
    if state not in (0, 1):
        raise ValueError("Invalid state")
    
    logging.info(f"Set output state: {state}")
    
    command = f"OP1 {state};*OPC?"
    power_supply.query(command)
