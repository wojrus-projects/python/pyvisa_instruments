"""
DMM Keithley DMM6500.

Source: DMM6500 Reference Manual (DMM6500-901-01 Rev. B / September 2019)
"""

import logging

import pyvisa as visa

# Script globals.
multimeter = None

# Instrument parameters.
DIGITIZER_SAMPLE_RATE_MIN_Hz = 1_000
DIGITIZER_SAMPLE_RATE_MAX_Hz = 1_000_000

def initialize_multimeter(visa_resource_manager: visa.ResourceManager, visa_resource_name: str) -> None:
    """
    Initialize instrument.
    """
    global multimeter

    logging.info("Open VISA resource: " + visa_resource_name)
    multimeter = visa_resource_manager.open_resource(visa_resource_name)
    multimeter.timeout = 10000
    multimeter.read_termination = ""
    multimeter.write_termination = ""
    multimeter.chunk_size = 100 * 1024
    multimeter.clear()

    logging.info("Query IDN")
    idn = multimeter.query("*IDN?")
    logging.info("Device IDN: " + idn)

def set_range(function: str, measure_range: float) -> None:
    """
    Set the positive full-scale measure range.
    
    function:
        VOLTage[:DC]
        VOLTage:AC
        CURRent[:DC]
        CURRent:AC
        RESistance
        FRESistance
        CAPacitance
        VOLTage[:DC]:RATio
        
    range:
        You can assign any real number. The instrument selects the closest fixed range
        that is large enough to measure the entered number. For example, for current measurements, if you
        expect a reading of approximately 9 mA, set the range to 9 mA to select the 10 mA range.
    """
    measure_range = float(measure_range)
    
    command = f":SENSe:FUNCtion \"{function}\""
    multimeter.write(command)
    
    command = f":SENSe:{function}:RANGe {measure_range}"
    multimeter.write(command)

def measure(function: str = None) -> float:
    """
    Makes measurements, places them in a reading buffer, and returns the last reading.

    function:
        VOLTage[:DC], VOLTage:AC, CURRent[:DC], CURRent:AC,
        RESistance, FRESistance, DIODe, CAPacitance,
        TEMPerature, CONTinuity, FREQuency[:VOLTage], PERiod[:VOLTage],
        VOLTage[:DC]:RATio.

        This function remains selected after the measurement is complete.
        If you do not define the function parameter, the instrument uses the presently selected measure
        function. If a digitize function is presently selected, an error is generated.
    """
    if function is None:
        command = ":MEASure?"
    else:
        command = f":MEASure:{function}?"

    value = multimeter.query(command)

    return float(value)

def measure_digitize(function: str = None) -> float:
    """
    Makes a digitize measurement, places it in a reading buffer, and returns the reading.

    function:
        VOLTage, CURRent.

        If no function is defined, the presently selected one is used
    """
    if function is None:
        command = ":MEASure:DIGitize?"
    else:
        command = f":MEASure:DIGitize:{function}?"

    value = multimeter.query(command)

    return float(value)

def trace_trigger_digitize(buffer_name: str = None) -> None:
    """
    This command makes readings using the active digitize function and stores them in the reading buffer.
    """
    command = ":TRACe:TRIGger:DIGitize"
    if buffer_name is not None:
        command += f' "{buffer_name}"'

    multimeter.write(command)

def trace_actual(buffer_name: str = None) -> int:
    """
    Return: number of readings in the specified reading buffer.
    """
    command = ":TRACe:ACTual?"
    if buffer_name is not None:
        command += f' "{buffer_name}"'

    value = multimeter.query(command)

    return int(value)

def trace_actual_index(buffer_name: str = None) -> tuple:
    """
    Return: starting and last index in a reading buffer.
    """
    if buffer_name is not None:
        command = f':TRACe:ACTual:STARt? "{buffer_name}";END? "{buffer_name}"'
    else:
        command = ":TRACe:ACTual:STARt?;END?"

    start_index, end_index = multimeter.query_ascii_values(command, separator=';', converter='d', container=tuple)

    return (start_index, end_index)

def trace_data(start_index: int, end_index: int, buffer_name: str = None) -> list:
    """
    Returns specified data elements from a specified reading buffer.

    start_index:
        Beginning index of the buffer to return; must be 1 or greater.

    end_index:
        Ending index of the buffer to return.

    buffer_name:
        A string that indicates the reading buffer; the default buffers (defbuffer1 or
        defbuffer2) or the name of a user-defined buffer; if no buffer is specified,
        defbuffer1 is used.

    Return:
        Measured data (list of floats).
    """
    start_index = int(start_index)
    end_index = int(end_index)

    if start_index < 1:
        raise ValueError("Invalid start_index")

    if end_index < start_index:
        raise ValueError("Invalid end_index")

    command = f":TRACe:DATA? {start_index}, {end_index}"
    if buffer_name is not None:
        command += f', "{buffer_name}"'

    data = multimeter.query_ascii_values(command, separator=',', converter='f', container=list)

    return data

def trace_data_all(buffer_name: str = None) -> list:
    """
    Returns all data elements from a specified reading buffer.
    """
    start_index, end_index = trace_actual_index(buffer_name)

    command = f":TRACe:DATA? {start_index}, {end_index}"
    if buffer_name is not None:
        command += f', "{buffer_name}"'

    data = multimeter.query_ascii_values(command, separator=',', converter='f', container=list)

    return data

def trace_clear(buffer_name: str = None) -> None:
    """
    Clears all readings and statistics from the specified buffer.

    buffer_name:
        A string that indicates the reading buffer; the default buffers (defbuffer1 or
        defbuffer2) or the name of a user-defined buffer; if no buffer is specified,
        defbuffer1 is used
    """
    command = ":TRACe:CLEar"
    if buffer_name is not None:
        command += f' "{buffer_name}"'

    multimeter.write(command)

def display_light_state(brightness: str) -> None:
    """
    Set LCD brightness:
        - Full brightness: ON100
        - 75% brightness:  ON75
        - 50% brightness:  ON50
        - 25% brightness:  ON25
        - Display off:     OFF
        - Display and all indicators off: BLACkout
    """
    multimeter.write(f":DISPlay:LIGHt:STATe {brightness}")

def display_user_text(line: int, text: str) -> None:
    """
    Defines the text that is displayed on the front-panel USER swipe screen.

    line:
        The line of the USER swipe screen on which to display text:
        - Top line: 1
        - Bottom line: 2

    text:
        String that contains the message; up to 20 characters for top line and 32 characters
        for bottom line.
    """
    line = int(line)
    if line == 1:
        TEXT_LENGTH_MAX = 20
    elif line == 2:
        TEXT_LENGTH_MAX = 32
    else:
        raise ValueError("Invalid text length")

    if len(text) > TEXT_LENGTH_MAX:
        text = text[0 : TEXT_LENGTH_MAX]

    multimeter.write(f':DISP:USER{line}:TEXT "{text}"')

def display_clear() -> None:
    """
    Clears the text from the front-panel USER swipe screen.
    """
    multimeter.write(":DISPlay:CLEar")

def display_buffer_active(buffer_name: str) -> None:
    """
    Select measurement buffer.
    """
    multimeter.write(f':DISPlay:BUFFer:ACTive "{buffer_name}"')

def display_screen(screen_name: str = "HOME") -> None:
    """
    Select screen.

    screen_name:
        - Home screen: HOME
        - Home screen with large readings: HOME_LARGe_reading
        - Reading table: READing_table
        - Graph screen (opens last selected tab): GRAPh
        - Histogram screen: HISTogram
        - FUNCTIONS swipe screen: SWIPE_FUNCtions
        - GRAPH swipe screen: SWIPE_GRAPh
        - SECONDARY swipe screen: SWIPE_SECondary
        - SETTINGS swipe screen: SWIPE_SETTings
        - STATISTICS swipe screen: SWIPE_STATistics
        - USER swipe screen: SWIPE_USER (only displays USER swipe screen if user text is sent)
        - CHANNEL swipe screen: SWIPE_CHANnel
        - NONSWITCH swipe screen: SWIPE_NONSwitch
        - SCAN swipe screen: SWIPE_SCAN
        - Channel control screen: CHANNEL_CONTrol
        - Channel settings screen: CHANNEL_SETTings
        - Channel scan screen: CHANNEL_SCAN
        - Open a screen that uses minimal CPU resources: PROCessing
    """
    multimeter.write(f":DISPlay:SCReen {screen_name}")

def digitize_voltage(sample_rate: int, sample_count: int) -> float:
    """
    Funkcja włącza próbkowanie napięcia, czeka na koniec i zwraca ostatnią, jedną próbkę.\n
    Wszystkie lub fragment danych z pamięci digitizera można pobrać za pomocą `trace_data()` lub `trace_data_all()`.
    """
    sample_rate = int(sample_rate)
    sample_count = int(sample_count)

    if sample_rate < DIGITIZER_SAMPLE_RATE_MIN_Hz:
        sample_rate = DIGITIZER_SAMPLE_RATE_MIN_Hz
    elif sample_rate > DIGITIZER_SAMPLE_RATE_MAX_Hz:
        sample_rate = DIGITIZER_SAMPLE_RATE_MAX_Hz

    command = ':DIGitize:FUNC "VOLTage"'
    command += ';:DIGitize:VOLTage:APERture AUTO'
    command += f';:DIGitize:VOLTage:SRATe {sample_rate}'
    command += f';:DIGitize:COUNt {sample_count}'

    multimeter.write(command)

    command = ":MEASure:DIGitize?"

    data = multimeter.query_ascii_values(command, separator=',', converter='f', container=list)

    return data[0]
